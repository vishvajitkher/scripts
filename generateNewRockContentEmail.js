/**
 * Client tool email list uses
 *
 * @returns Generates a new list with scribblive and rockcontent emails
 */
function generateNewRockContentEmail() {
  const emails = 'aneeq.khan@scribblelive.com,long.do@scribblelive.com,alexa.wilson@scribblelive.com,melanie.falloretta@scribblelive.com,casey.knox@scribblelive.com,max.weissler@scribblelive.com,andrew.tweeton@scribblelive.com,david.kavalsky@scribblelive.com,paul.evora@scribblelive.com,andrey@scribblelive.com,gerd.paul@scribblelive.com,brett.huntington@scribblelive.com,ahmed.mohamed@scribblelive.com';
  if (emails && emails.trim().length) {
    const emailList = emails.split(',');
    const result = new Set();
    if (emailList && emailList.length) {
      emailList.forEach((i) => {
        const scribbleliveEmail = '@scribblelive.com';
        const rockcontentEmail = '@rockcontent.com';
        if (i.endsWith(scribbleliveEmail)) {
          result.add(i.trim().toLowerCase());
          result.add(i.replace(scribbleliveEmail, rockcontentEmail).trim().toLowerCase());
        } else {
          result.add(i.trim().toLowerCase());
        }
      });
    }
    if (result && result.size) {
      return Array.from(result).join(',');
    } else {
      return '';
    }
  }
}

console.log(generateNewRockContentEmail());